import http from 'http'
import fs from 'fs'
import path from 'path'



const httpServer=http.createServer(((req,res)=>{
console.log(`req: ${req.url}`)

if(req.url ==='/'){
    sendRes('index.html','text.html',res)
}
else if(/\/uploads\/[^\/]+$/.test(req.url) && req.method ==='POST'){

}
else{
    sendRes(req.url,getContentType(req.url),res)
}
}))


function sendRes(url, contentType,res){
    let file= path.join(__dirname+'/static',url)
    fs.readFile(file,(err, content)=>{
        if(err){
            res.writeHead(404)
            res.write('File not found');
            res.end()
            console.log('error 404')
        }
        else{
            res.writeHead(200,{'Content-Type':contentType})
            res.write(content);
            res.end()
            console.log(`res 200 ${file}`)
        }
    })
}


// content-tyoe
function getContentType(url){
    switch (path.extname(url)){
        case ".html":
            return 'text/html';
        case ".css":
            return "text/css";
        case ".js":
            return "application/javascript"; 
        case ".json":
                return "application/json"; 
        default:
            return "application/octate-stream"        

    }
}


httpServer.listen(3000,()=>{
    console.log('node.js port 3000')
})
