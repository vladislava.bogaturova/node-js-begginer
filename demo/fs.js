//File system
import fs from 'fs'
import path from 'path'
import { fileURLToPath } from 'url';

const __filename = fileURLToPath(import.meta.url);

const __dirname = path.dirname(__filename);

// fs.mkdir(path.join(__dirname, 'test'),(error)=>{
// if(error){
//     throw error
// }
// console.log('folder was created')
// })

const filePath=path.join(__dirname, 'test','text.txt')

// fs.writeFile(filePath, 'Hello Node JS', (err)=>{
// if(err){
//     throw err
// }
// console.log('File was created')

// fs.appendFile(filePath, '\nHello again', (err)=>{
//     if(err){
//         throw err
//     }
// console.log('File was created')
// })
// })

fs.readFile(filePath,'utf-8', (err,content)=>{
    if(err){
        throw err
    }
    console.log(content)
    // const data =Buffer.from(content)
    // console.log('content: ', data.toString())
})